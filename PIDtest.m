set(0, "defaultaxesfontsize", 16)  % axes labels
Kp = 50;
Ki = 0;
Kd = 100;
m = 10;

nbsamples = 1000;
r = 0;
y = 10;
dt = 0.01;
err = [0, r-y];
yhist = [];
v = 0;
for i = 1:nbsamples
    err(i) = r-y;
    u = Kp*err(i) + Ki*sum(err) + Kd*diff(err)(end);
    v = v + u/m*dt;
    y = y + v*dt;
    yhist(i) = y;
endfor
t = [1:nbsamples]*dt
plot(t,yhist)
hold on
D = Kd^2-4*m*Kp
P1 = sqrt(D)/(2*m)-Kd/(2*m)
P2 = -P1
plot(t,1/(2*m*sqrt(D))*((Kd^2-2*m*Kp)*(e.^(t*P1)-e.^(t*P2))+Kd*sqrt(D)*(e.^(t*P1)+e.^(t*P2))))
hold off
legend("Numerisk","Symbolsk")
xlabel("Tid")
ylabel("PV")
grid on
