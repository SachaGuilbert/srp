Titel:
Modellering af en drones dynamik mhp. at skabe et 0g laboratorie

Problemformulering:
Undersøg ved hjælp af matematisk modellering hvordan en drones dynamikker i teorien kan udnyttes til at skabe vægtløshed. Undersøg ved hjælp af en prototype hvilke praktiske udfordringer man kan forvente.

Arbejdsspørgsmål - Matematisk model:
 * Redegør for de kræfter der påvirker en drone
 * Hvordan opfører en drone sig uden styring?
 * Hvordan fungerer styring ved hjælp af feedback-loop?
 * Undersøg stabilitetsforhold afhængigt af styringsparameterne teoretisk og praktisk
 * Hvordan påvirker luftmodstanden dronens frie fald?
 * Hvordan kan man sørge for at dronens acceleration er 9.82m/s² på trods af luftmodstanden?

Arbejdsspørgsmål - Implementering i praksis:
 * Valg af teknologi - styresystem(arduino), accelerometer(IMU), implementering i kode(c++) - stepwise improvement(use-, modify-, create- progression), vægtminimering(jf. læreplanen for rumfysik, fysik i det 21. århundrede)
 * Observering af dronens opførsel afhængigt af styringsparameterne
 * Testflyvninger
 * Opsamling af data

Pilotprojekt:
Op til SRP'en foretages et pilotprojekt til at afklare hvorvidt det er realistisk at:
 * beskrive den fysiske drones opførsel ved hjælp af en differentialligning
 * om den givne droneplatform kan konfigureres til at levere de nødvendige data til en decideret dataanalyse
