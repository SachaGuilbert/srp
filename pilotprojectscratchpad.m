ypr = [0,1,0]';
yprp = [0,0,0]';
motorpowers= [0,0,0,0]';

%y = 0; # flat        positvt set oppefra: ccw
%p = 0; # fremad      positvt: næsen ned
%r = 0; # til siden   positvt: cw

kp = 1.0;
kd = 2.0;

m = 0.1;

yprHist = [];
force = zeros(3,1);
nbsamples = 600;
dt = 1/20;
prevForce = [0 0 0]';
for i=1:nbsamples,
    errToAct =  [1, -1, 1
                -1, -1, -1
                1, 1, -1
                -1, 1, 1];
    motorpowers= -errToAct*(kp*ypr + kd*yprp*dt);
    responseMatrix = errToAct * inv(errToAct ' * errToAct);
%    force = [1,-1,1,-1
%            -1,-1,1,1
%            1,-1,-1,1] *motorpowers;
    force = responseMatrix' * motorpowers;
    yprp = yprp + 1/m*prevForce*dt; %+ randn(3,1);
    ypr = ypr + yprp*dt;
    yprHist(:,end+1) = ypr;
    prevForce = force;
endfor
t = (1:nbsamples)*dt;
plot(t,yprHist(2,:)', '-x')
hold on;
omega = sqrt(kp/m)
alpha = -kd / m / 20 / 2 + 1/20/2*omega^2;
plot(t,exp(alpha*t).*cos(omega*t))
hold off
legend("Drone","Model")
%legend("y","p","r")
