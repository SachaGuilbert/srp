Pilotprojekt
============

Op til SRP'en foretages et pilotprojekt til at afklare hvorvidt det er realistisk at:
 * beskrive den fysiske drones opførsel ved hjælp af en differentialligning
 * om den givne droneplatform kan konfigureres til at levere de nødvendige data til en decideret dataanalyse

Teoretisk del:
-------------
I pilotprojektet har vi undersøgt hvorvidt man en enkel differentialligning med formen a*x'' = F kan beskrive dronens dynamik. I praksis har vi gjort dette ved at implementere en [numerisk model i octave](pilotprojectscratchpad.m).
Dronen kan beskrives ved en stokastisk differentialligning hvor accelerationen er den stokastiske variabel
Modellen viser at dronen i udgangspunktet er ustabil som vist i grafen nedenfor:

![Open loop](openloop.png)

Hvis F er en stokastisk variabel som repræsenterer støj, ses det af grafen, at dronens *attitude* (yaw, pitch, roll) divergerer væk fra  0 og dronen er derfor ustabil. Som forventet kræver dronen derfor styring ved hjælp af et feedback-loop (grafen viser hvordan pitch udvikler sig over tid):

![Feedback loop](PIDcontrolled.png)

Hvis man til gengæld vælger styringsparametrene forkert er dronen også ustabil:

![Wrong parameters](wrongparameters.png)

Noget tyder på at dronens opførsel kan beskrives ved en eksponentiel cosinus funktion e^(ɑx)cos(ωx)

Praktisk del:
------------
I den praktiske del prøver vi at finde ud af hvor svært det vil være at udføre projektet i praksis. For det første spørgsmål er hvor mange sekunders 0g vi kan forvente at opnå, ved hjælp af selve konceptet. Til det formål har vi undersøgt verdensrekorden for hurtigste vertikale stigning som er angivet i denne video (4:20):
https://www.youtube.com/watch?v=GUk8Etzs69Q

Med en verdensrekord drone som kan opnå hastigheder på 189km/t eller 52.5m/s vertikalt, kan man opnå ca. 5 sekunders 0g indtil dronen begynder at falde igen og derefter yderligere 5-10 sekunder under faldet, hvor 10 nok er meget optimistisk. Bemærk, at man på vej ned vil skulle overkomme luftmodstanden, hvilket burde kunne lade sig gøre ved at dreje propellerne den anden vej. Det vil dog kræve omprogrammering af ESC'erne(*electronic speed controller*) og det er tvivlsomt om dette kan nås i projektet.
Vores drone kommer nok ikke op i de hastigheder.
Det andet spørgsmål er om vi faktisk kan trække data ud af dronen og programmere den til det tænkte formål.
Her er en video af dronen under flyvning og data på pitch og roll plottet:

![Data006](plotdata6.png)

[Video af flyvning](data6.mp4)

Vi kan altså trække data ud af dronen, men spørgsmålet er om ikke vi skulle kalibrere PID loopet yderligere for at opnå en tilstrækkelig stabilitet.
