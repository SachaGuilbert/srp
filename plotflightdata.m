function D = plotflightdata(dataFileName)
D = dlmread(dataFileName)
t = D(:,1)
subplot(2,1,1)
hold off
plot(t,D(:,2:3))
grid on
subplot(2,1,2)
hold off
plot(t,D(:,end))
hold off
endfunction
